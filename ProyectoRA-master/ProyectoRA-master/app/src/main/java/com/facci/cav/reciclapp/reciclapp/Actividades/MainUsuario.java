package com.facci.cav.reciclapp.reciclapp.Actividades;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import com.facci.cav.reciclapp.reciclapp.Modelo.Usuario;
import com.facci.cav.reciclapp.reciclapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainUsuario extends AppCompatActivity {
    private EditText Usuario, Nombres, Apellidos, NumberPhone, Direccion, Password, VerifPassword;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference usuario;
    private String userId;
    private FirebaseAuth.AuthStateListener authStateListener;
    private FirebaseAuth firebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_usuario);
        Usuario = (EditText) findViewById(R.id.txtMPFusuario);
        Nombres = (EditText) findViewById(R.id.txtMPFNombre);
        Apellidos = (EditText) findViewById(R.id.txtMPFpellido);
        NumberPhone = (EditText) findViewById(R.id.txtMPFTelefono);
        Direccion = (EditText) findViewById(R.id.txtMPFDireccion);
        Password = (EditText) findViewById(R.id.txtMPFContaseña);
        VerifPassword = (EditText) findViewById(R.id.txtMPFCContaseña);
        firebaseDatabase = FirebaseDatabase.getInstance();
        usuario = firebaseDatabase.getReference();
        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        userId = user.getUid();

        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

            }
        };

        usuario.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Bienvenido(dataSnapshot);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void Bienvenido(DataSnapshot dataSnapshot){
        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
            Usuario usuario1 = new Usuario();
            usuario1.setNombre(dataSnapshot1.child(userId).getValue(Usuario.class).getNombre());
            usuario1.setApellido(dataSnapshot1.child(userId).getValue(Usuario.class).getApellido());
            usuario1.setCorreo(dataSnapshot1.child(userId).getValue(Usuario.class).getCorreo());
            usuario1.setPassword(dataSnapshot1.child(userId).getValue(Usuario.class).getPassword());
            usuario1.setDireccion(dataSnapshot1.child(userId).getValue(Usuario.class).getDireccion());
            usuario1.setTelefono(dataSnapshot1.child(userId).getValue(Usuario.class).getTelefono());
            usuario1.setVerificacionPassword(dataSnapshot1.child(userId).getValue(Usuario.class).getVerificacionPassword());

            Usuario.setText(usuario1.getCorreo());
            Nombres.setText(usuario1.getNombre());
            Apellidos.setText(usuario1.getApellido());
            NumberPhone.setText(usuario1.getTelefono());
            Direccion.setText(usuario1.getDireccion());
            Password.setText(usuario1.getPassword());
            VerifPassword.setText(usuario1.getVerificacionPassword());


        }
    }
}

