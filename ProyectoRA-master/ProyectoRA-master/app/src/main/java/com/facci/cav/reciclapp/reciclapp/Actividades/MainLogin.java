package com.facci.cav.reciclapp.reciclapp.Actividades;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facci.cav.reciclapp.reciclapp.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainLogin extends AppCompatActivity {
    private Button btnRegistro;
    private Button Accede;
    private EditText usuarioLogin, passwordLogin;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener whatevername;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_login);
        btnRegistro = (Button)findViewById(R.id.BtnRegistrarse);
        Accede = (Button)findViewById(R.id.BtnAcceder);
        usuarioLogin = (EditText) findViewById(R.id.txtusuario);
        passwordLogin = (EditText) findViewById(R.id.txtContaseña);
        mAuth= FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);
        whatevername = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if(firebaseAuth.getCurrentUser()!=null){
                    Intent intent = new Intent(MainLogin.this, MainMenu.class);
                    startActivity(intent);
                    finish();

                }else {

                }
            }
        };

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainLogin.this, MainRegistroUser.class);
                startActivity(intent);
            }
        });
        Accede.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(usuarioLogin.getText().toString().isEmpty()){
                   usuarioLogin.setError(getString(R.string.campoUsuarioRegistrado));
               }else if(passwordLogin.getText().toString().isEmpty()){
                   passwordLogin.setError(getString(R.string.campoPasswordUsuario));
               }else{
                   String usuario = usuarioLogin.getText().toString();
                   String password = passwordLogin.getText().toString();

                   progressDialog.setTitle(getString(R.string.mensajeConsultando));
                   progressDialog.setMessage(getString(R.string.mensajeLogin));
                   progressDialog.show();
                   mAuth.signInWithEmailAndPassword(usuario,password).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                       @Override
                       public void onSuccess(AuthResult authResult) {
                           Intent intent = new Intent(MainLogin.this, MainMenu.class);
                           startActivity(intent);
                           finish();
                       }
                   }).addOnFailureListener(new OnFailureListener() {
                       @Override
                       public void onFailure(@NonNull Exception e) {
                           Toast.makeText(MainLogin.this, getString(R.string.mensajeErrorLogin), Toast.LENGTH_LONG).show();
                           progressDialog.hide();
                       }
                   });


               }
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(whatevername);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (whatevername!=null){
            mAuth.removeAuthStateListener(whatevername);
        }
    }
}