package com.luis.auz.semana_actual;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    TextView aprobado, reprobado;
    int valor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        aprobado = (TextView) findViewById(R.id.lbl_Aprobado);
        reprobado = (TextView) findViewById(R.id.lbl_Reprobado);

        valor = Integer.parseInt(getIntent().getStringExtra("llave_obtener"));
        Calendar calendar = Calendar.getInstance();
        if (valor==calendar.get(Calendar.WEEK_OF_YEAR)){
            aprobado.setText("HAS ACERTADO");
        }
        else{
            reprobado.setText("INTENTA NUEVAMENTE");
        }
    }
}
