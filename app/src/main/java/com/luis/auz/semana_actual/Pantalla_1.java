package com.luis.auz.semana_actual;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Pantalla_1 extends AppCompatActivity {

    EditText editText_numeroSemana;
    Button btn_revisar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_1);

        editText_numeroSemana = (EditText) findViewById(R.id.numeroSemana);
        btn_revisar = (Button) findViewById(R.id.btn_revisar);
        //Sirve para escuchar el boton(abajo)
        btn_revisar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText_numeroSemana.getText().toString().isEmpty()){
                    editText_numeroSemana.setError(getString(R.string.mensaje_log));
                }
                else {
                    Intent intent = new Intent(Pantalla_1.this,
                            MainActivity.class);
                    //Creas el objeto Intent para movilizar entre actividades
                    intent.putExtra("llave_obtener",
                            editText_numeroSemana.getText().toString());
                    //llevar el texto escrito en el edit Text a la otra actividad
                    startActivity(intent);
                    Log.e("Numero", editText_numeroSemana.getText().toString()+" Enviado");
                    //Abre la segunda pantalla
                    editText_numeroSemana.setText("");
                    //Limpiar el edit text
                }
            }
        });

    }
}
